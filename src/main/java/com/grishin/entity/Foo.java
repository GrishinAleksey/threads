package com.grishin.entity;

import java.util.concurrent.CountDownLatch;

public class Foo {

    public volatile CountDownLatch START = new CountDownLatch(3);
    private final Object firstLock = new Object();
    private final Object secondLock = new Object();

    public void first() {
        try {
            START.countDown();
            START.await();
            System.out.print("first");
            synchronized (firstLock) {
                firstLock.notify();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void second() {
        try {
            START.countDown();
            START.await();
            synchronized (firstLock) {
                firstLock.wait();
            }
            System.out.print("second");
            synchronized (secondLock) {
                secondLock.notify();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void third() {
        try {
            START.countDown();
            START.await();
            synchronized (secondLock) {
                secondLock.wait();
            }
            System.out.print("third");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
