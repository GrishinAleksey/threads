package com.grishin;
import com.grishin.entity.Foo;

import java.util.ArrayList;
import java.util.List;

public class Test {
    public static void main(String[] args) {
        Foo foo = new Foo();

        List<Thread> runnables = new ArrayList<>();
        runnables.add(new Thread(foo::first));
        runnables.add(new Thread(foo::third));
        runnables.add(new Thread(foo::second));

        runnables.parallelStream().forEach(Thread::start);
    }
}
